## Directions to view MD Book
1. Download Rust
    - <https://www.rust-lang.org/tools/install>
2. Open command prompt
3. Run `cargo install mdbook`
4. In this repo, navigate to the "MD Files" directory
5. Run `mdbook build -o`
