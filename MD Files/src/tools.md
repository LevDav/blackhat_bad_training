## **Exploit Tools**

### **Tools preinstalled in Kali Linux:**
- `nmap`
- `nbtscan`
- `hashcat`

### **Tools to download:**
- Responder
    - <https://github.com/lgandx/Responder>
- BloodHound
    - <https://github.com/BloodHoundAD/BloodHound>
- Wmiexec
    - <https://github.com/SecureAuthCorp/impacket>
- Procdump
    - <https://docs.microsoft.com/en-us/sysinternals/downloads/procdump>
- pypykatz
    - <https://github.com/skelsec/pypykatz>
