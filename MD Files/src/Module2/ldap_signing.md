## **LDAP Signing and Channel Binding**

### **What are LDAP Signing and Channel Binding?**
LDAP signing is when digital signing of LDAP traffic is required from the source. This guarantess authenticity just as it would when hand signing a document manually.

LDAP Channel Binding is binidng the transport and application layer together, creating a unique finger print for the LDAP communication that cannot be reused even if intercepted.

These two protocols are ways to increase security between LDAP clients and their communication between each other and the AD DC. Without both, vulnerabilities are open that can lead to illegal user privilege elevation.

*By default, LDAP signing and channel binding are not enabled.*

