## **Relay Attacks**

### **What is a Relay Attack?**
A relay attack is a similar to a man-in-the-middle (MITM) attack. In a relay attack, an attacker intercepts communication between two parties and immediately "relays" the information to another device without changing or manipulating any data. This can all be done without initiating communication between the two original parties.

### **Exploitation**
For the purposes of this module, the information received in this attack is usually credentials which are then placed in a new set of instructions of the attackers choosing, and then sent to the original destination.

***NTLM RelayX***

NTLM relay is a technique of standing between a client and a server to perform actions on the server while impersonating the client. 

Specifically for a NTLM Relay, the user will ask for a service from the domain controller, in the process giving the hash of its password. The attacker, sitting in the middle, will intercept this message, and pretend to be the user, stealing the password hash to authenticate as the legitimate user. The DC will then verify the attacker thinking it is the original user. At this point, the attacker can ask for whatever it wishes.

It can be very powerful and can be used to take control of an Active Directory domain from a black box context (no credentials). ​