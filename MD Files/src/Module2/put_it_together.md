## **Exploiting with Dynamic DNS and LDAP Signing Disabled**

While enabling LDAP signing will prevent MITM attacks, because it is not automatically enabled, many organizations may forget to turn it on, thus leaving the AD open to attacks.

**Exploitation**

Suppose your AD has a cake service on the network. Any user within the AD can ask for a cake, and with proper authentication (password hash), will be granted a cake.

In the background, an attacker uses dynamic DNS to send a new DNS entry for the cake service to the DC. All the users on the network are informed that the cake service has moved from its original location to the attackers computer.

The victim will then send its password hash to the attacker in an attempt to get a cake. Using an NTLM relay attack, the attacker will intercept these credentials and send them to the DC, asking for ultimate access.

Because the credentials sent to the DC are valid credentials, the DC grants what the attacker asks for, access to the AD.