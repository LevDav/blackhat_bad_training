# **Module 2**

## **Scenario:**
* Access to the internal network via a dropbox or a compromised machine
* No user credentials

## **Objective:**
Gain domain admin access

## **Concepts**
* [HTTP Misconfigurations](./http.md)
* [Dynamic DNS Injection](./dynamic_dns.md)
* [Relay Attacks](./relay_attacks.md)
* [LDAP Signing and Cheannel Binding](./ldap_signing.md)
* [Dynamic DNS + LDAP Signing Exploitation](./put_it_together.md)
* [DCSync](./dcsync.md)