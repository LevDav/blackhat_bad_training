## **DCSync**

### **What is DCSync?**
DCSync attacks allow an attacker to impersonate a domain controller and request password hashes from other domain controllers.​ They enable an attacker to target a domain controller without having to log on to or place code on the controller.​

It is slightly limited as only accounts that have certain replication permissions with Active Directory can be targeted and used in a DCSync attack.​

*The best defense against this attack is to monitor network traffic and controll replication permissions.*

​