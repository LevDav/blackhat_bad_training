## **Dynamic DNS Injection**

### **What is Dynamic DNS Injection?**
Dynamic DNS keeps DNS records automatically up to date when an IP address changes. This system is mainly used in large networks that host internal services and use internal DNS and DHCP (Dynamic Host Configuration Protocol) servers. But home networks and small companies can also take advantage of dynamic DNS.

A dynamic DNS injection happens when a malicious DHCP client enters a host name into the DNS maps. This is possible because you don't need AD credentials to change a DNS entry.

### **Exploitation**
An attacker can use `nmap` on a Linux system to try and gauge if a system is vulnerable to this attack. If it is, they can then use `nsupdate` to add their computer IP address to the DNS server.