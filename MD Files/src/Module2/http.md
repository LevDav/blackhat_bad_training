## **HTTP Misconfigurations**

### **Types of Misconfigurations**
***HTTP Servers***

Web servers can have misconfigurations that make them open to exploitation. Applications like Jenkins and Apache web servers often fall prey to these. Jenkins, for example allows RCE (remote code execution). In addition, internal applications have default credentials that could lead to footholds.

***Printers***

There are often printers that get configured with an AD account. For example, if a printer has a scanner and scanned documents are sent over email, in order to get a list of user emails in the domain, LDAP is needed. Because of this possibility, printers are a good way to get an AD account through LDAP.

### **Exploitation**
Due to the RCE vulnerability, the default credentials needed for internal applications can be gathered through LDAP in a domain computer. To obtain these credentials, an attacker can use `Responder.py` to poison LDAP messages and gain printer credentials. This allows for a foothold within the web applications.

Metasploit, a Linux application, can be used to find matching HTTP misconfiguration vulnerabilities.

*Once a foothold is reached, enumerate the network with BloodHound.*