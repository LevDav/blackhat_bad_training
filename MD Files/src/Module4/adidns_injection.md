## **ADIDNS Injection**

### **What is ADIDNS (AD-Integrated DNS) Injection?**
Similar to DNS zones, AD's have their own DNS zones that are accessible to AD objects. A user can create new DNS entries and edit those new entries. However, prior entries are unavailable for editing. 

### **Exploitation**
By being able to edit DNS entries, this leaves room for misconfigurations and vulnerability. For example, in some systems, WPAD and *(Wildcard) nodes aren't recorded in DNS records. This allows attackers to inject themselves, and whenever a user tries to contact WPAD or there is a typo in initial contact (causing a *(Wildcard)), they will pass off credentials to the attacker.

*When trying to pentest with this exploitation, take caution. If done incorrectly, the system **will** crash.*