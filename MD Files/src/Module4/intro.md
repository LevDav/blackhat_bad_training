# **Module 4**

## **Scenario:
* Access to the internal network via a dropbox or a compromised machine
* No user credentials

## **Objective**
Gain domain admin access

## **Concepts**
* [ADIDNS Injection](./adidns_injection.md)
* [Kerberos and Single Sign-On](./kerberos_sso.md)
* [Double Hop Problem](./double_hop.md)
* [Unconstrained Delegation](./unconstrained_delegation.md)
* [Print Spooler](./print_spooler.md)