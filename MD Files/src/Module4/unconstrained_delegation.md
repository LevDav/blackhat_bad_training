## **Unconstrained Delegation**

### **The Solution to the Double Hop Problem**
In order to solve the Double Hop problem, a system uses delegation. The job of authenticating a user in typical Kerberos is handled by the key distribution center (KDC)​. Each time the initial user wishes to gain access to another server it must be authenticated again through the KDC​. Delegation allows the job of the KDC to be done through the server​. 

### **How Unconstrained Delegation Works**
The process begins the same as SSO. However, once the user has been granted a session ticket for a service through the KDC, if delegation has been turned on, a copy of the user's ticket granting ticket is also sent to the service in addition to the session token​. This service can now request session tickets from the KDC without having the user re-authenticate themselves​. The service can get information from other services on behalf of the user​.

### **The Process**
*The user would like to gain access to the database, but needs a session token from the KDC which would require reauthentication​. To avoid reauthentication, delegation is needed​.*
![Delegation Step 1](../Pics/Delegation1.png)


*Delegation is set on the web server​. This negates the need for reauthentication and re-entering of credentials by the user.*
![Delegation Step 2](../Pics/Delegation2.png)

*When the user initially gains a session token for the web server, the KDC also sends a ticket granting ticket with it.*
![Delegation Step 3](../Pics/Delegation3.png)

*With this ticket granting ticket, the web server can now give the user a session token to the database without having to re-enter credentials.*
![Delegation Step 4](../Pics/Delegation4.png)

*The web server accesses the KDC directly to grant the user a session token for the database.*
![Delegation Step 5](../Pics/Delegation5.png)

*The user now has access to the database through the web server.*
![Delegation Step 6](../Pics/Delegation6.png)


### **Exploitation**
With unconstrained delegation comes the chance for expoitation if the user asking for authentication is the domain controller. When a compromised machine that has unconstrained delegation gets the DC to send its ticket granting ticket to use a service on that machine, the machine now has access to maliciously enumerate the network​. With this DC ticket granting ticket, an attacker can use DCSync​. DCSync can be used to retrieve anyone's credentials on the network.