## **Print Spooler**

### **What is Print Spooler?**
This is a protocol that Microsoft claims is 'Working as intended'. However, it is more of a bug than a working feature.

### **What does it do?**
Print Spooler runs the protocol 'RpcRemoteFindFirstPrinterChangeNotificationEx'. When running, it asks "Hey Service, could you notify me if something about the printer at <IP address> has changed?"

By default, this is active on all Windows Server machines (like domain controllers).

### **Exploitation**
#### **Easy Way:**

If the anti-viruses are disabled in a Windows machine, malicious .exe's can be uploaded to it.

Tools used to exploit:
* Rubeus.exe (To capture and extract the TGT)
* NetNTLMtoSilverTicket/dementor.py (To launch Print Spooler)
    * Use in a Linux workstation

#### **Hard Way:**

If you are not allowed to upload malicious .exe files onto a machine, then it becomes a little harder to exploit, but not impossible. 


***Service Principal Names (SPNs)***

For this scenario, Service Principal Names (SPNs) are deployed. This is just a way to identify the Machine (Domain object) of Services. It runs the same way as a DNS identifies the Address of Machines.

For example, if we had a computer on the network named `wdtp-bot1`, the following are several ways to resolve to this one machine.

| Service Principal Name     | Resolves to: |
| -------------------------- | ------------ |
| HOST\wdtp-bot1.wdtp.local  | wdtp-bot1    |
| HOST\wdtp-bot1             | wdtp-bot1    |
| WSMAN\wdtp-bot1.wdtp.local | wdtp-bot1    |
| WSMAN\WDTP-BOT1            | wdtp-bot1    |
| TERMSRV\WDTP-BOT1          | wdtp-bot1    |

When combined with the Print Spooler service, SPN is used twice, once to resolve the host, and once to resolve the DNS. If these two resolutions do not match, it causes issues.

***Exploiting***

To exploit this, an attacker (using wdtp-bot1's credentials) would add an SPN entry for wdtp-bot1. We'll use "HOST/attacker.wdtp.local" for our example. Then using anyone else credentials, the attacker will launch the Print Spooler Bug. This protocol will then look for updates to the printer at "HOST/attacker.wdtp.local", the malicious entry just added by the attacker.

The domain controller will check SPN for "HOST/attacker.wdtp.local" and see that it belongs to `wdtp-bot1`. The DC may also notice that if this bot has unconstrained delegation, that it needs to send its TGT when fulfilling the Print Spooler protocol.

The domain controller then checks the DNS for the hostname part of the SPN entry, "attacker.wdtp.local". However, because this SPN was made by the attacker, it does not exist. Nevertheless, with the help of an ADIDNS injection, the domain controller will redirect the entry to whichever computer was specified in the injection thinking that this malicious computer specified in the injection is `wdtp-bot1`. The DC will now ask the attackers computer if the printer has any updates while alos sending its TGT.

Tools used to exploit (all on a Linux system):
* addspn.py
* krbrelayx.py 
* printerbug.py
* secretsdump.py
