## **Double Hop Problem**

### **The Issue with Single Sign-On**
What happens when a user that signs on to an SSO must sign into another service through the one they are using? For example, a student administrator uses a website to enter student grades. The website needs to access the student database as the administrator, requiring the student admin to sign in and send credentials yet again.

**This is known as the double hop problem.**

![alt text](../Pics/Double_Hop.png)


​