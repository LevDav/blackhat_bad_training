## **Kerberos and Single Sign-On**

### **Purpose of Single Sign-On**
When logging into a computer, a user must send credentials across a network. When logging to multiple services on that network, a user must send those credentials multiple times. Every time they are sent, it increases the security risks.

Single Sign-On (SSO) allows a user to send credentials once. Once sent and authorized, the user has access to all the services needed. 

### **Kerberos**
Although, there are many SSO services, Kerberos is an older version of this service. The process for Kerberos is as follows:

![Kerberos Single Sign-On Process](../Pics/Kerberos_SSO.png "Kerberos Single Sign-On Process")

**Meaning of symbols:**


Symbol                                       | Meaning
-------------------------------------------- | -----------------------------------------------------
![Ticket Granting Ticket](../Pics/TGT.png)   | Encrypted with user password
![Session Token](../Pics/Session_Ticket.png) | Encrypted with the password of matching color service

1. A user sends a password/credentials to a Key Distribution Center (KDC). *In this case, it is just the domain controller.*
2. The user is authenticated through the Authentication service in the domain controller, and is granted a session token for the sign in session.
    * The user is given a ticket granting ticket (TGT) encrypted with their password from the ticket granting service (TGS) to use when trying to gain access to other services within the SSO.
3. The user then asks the KDC for a help authenticating into the service they would like to use. They send their TGT along with this request. This helps the SSO verify a valid user.
4. The KDC sends a session token for the service that is encrypted with the password of the service for the user back to the user.
5. The user can now be authenticated into the service they wish to use.




