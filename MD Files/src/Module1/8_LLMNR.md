## **LLMNR**

### **What is LLMNR?**
Link-Local Multicast Name Resolution (LLMNR) is a host discover/identification method for AD’s that locates hosts in the same local link or network​. When AD DNS is not feasible because the DNS server is unable to locate a device, LLMNR is used.

### **How does LLMNR work?**
When CPU1 is looking to connect on the same network as CPU2, if the IP address of CPU2 is not found within the DNS server, LLMNR will mulitcast to the entire domain looking for CPU2. When CPU2 is found, CPU1's credentials are taken and the connection is made.

### **Exploitation**
LLMNR has the potential to be used for exploitation. In the same scenario as earlier, if a malicious computer is on the network claims to be the "missing" computer, it can gain the credentials of a valid user. It now has legal access to the domain.

### **Enumeration**
Using tools such as Responder, we can spoof LLMNR and gain credentials from the domain.