## **Windows Hashes**
### **What is a hash?**
A hash is a one way, irreversible conversion of information​. In the case of an active directory, hashed passwords are our main concern.

There are many types of hashes:
* MD5
* SHA-256, SHA-512, SHA-1, etc.
* CRC32

### **Windows Hashes**
Windows stores passwords as NTLM hashes.

An example of NTLM hash
```aad3b435b51404eeaad3b435b51404ee:e19ccf75ee54e06b06a5907af13cef42​```
* The first half (before the colon) is the LM part of the hash
* The second half (after the colon) is the NT hash

Newer versions of Windows also use NTLMv1/v2. While these are still NTLM hashes, they use a more secure algorithm to hash credentials. 

### **Note:**
**Having a hash of a user password has the potential to gain access to that users account​**