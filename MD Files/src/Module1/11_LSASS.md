## **LSASS**
### **What is LSASS?**
LSASS (Local Security Authority Subsystem Service) is a process that stores domain, local usernames and passwords​. It provides a security policy and authentication to access within the Windows system​. Thus all the information it stores (domains, local usernames, passwords, etc.) are encrypted with LsaProtectMemory.

*Only those with administrative privileges can access this process*

### **Procdump**
This is a Windows application used to dump process memory for any process running on the system. This functionality is usually used to debug issues with specific problems. But because LSASS is a process, procdump can also be used to create memory dumps of it.

### **Enumeration/Explotiation**
Because a procdump of LSASS is possible, this means LSASS can be enumerated and exploited, potentially gaining access as a DA user. Several tools exist to perform such a task.

Tools to enumerate:
* `wmiexec`
*  `procdump`
    * Both tools are used to gather the LSASS process and dump it to your local system

Tools to exploit:
* `pypykatz`/`mimikatz`
    * These tools can translate the information in the dumped LSASS (hashes of passwords) into plain text. 