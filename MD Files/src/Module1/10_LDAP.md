## **LDAP**
### **What is LDAP?**
LDAP (Lightweight Directory Access Protocol) is a database that stores the objects within an AD and is used to provide communication between applications and directory service servers​. It allows a user to search across an entire network for a specific object without specifically knowing the name of an object or where it is located. 

### **Enumeration of LDAP**
LDAP can be used to see the structure of an Active Directory and how all the objects are connected to one another​. Finding the right path can gain you Domain Admin privileges​.

Tools to enumerate:
* SharpHound/BloodHound
    * SharpHound gathers all the information used with LDAP
    * Bloodhound is a visual representation of the directory structure and information gathered via SharpHound