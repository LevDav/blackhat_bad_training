## **Name Resolution**

### **Naming a computer on the domain**
When a computer is assigned to a domain, by default, the computer gives itself a name a mixture of the computer name plus the name of the AD domain it has joined​.

For example:
* Domain name: *wdtp.local*
* Workstation Name: *WDTP-BOT1*
* Computer Domain Name: *WDTP-BOT1.wdtp.local*

By having similar names, it is easier for users to identify what computers are on which domain.

### **What is a name resolution?**
This system is used by AD clients to locate AD DC and  other services on the network or by DC’s to communicate with one another​. 

### **What's the purpose of name resolution?**
Like when a Domain Name System (DNS) must change a website name to an IP address, a domain DNS finds the correct domain clients by querying the DNS server to discover domain controllers and resolve computer names to IP addresses​.