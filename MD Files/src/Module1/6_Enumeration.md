## **Enumeration of a Windows AD**
### **What is enumeration?**
Enumeration is simply finding as much information about an AD as possible. 
### **What do we enumerate?**
Things that are typically searched for in this scenario are:
* Domain controller
* Domain name 
* Hosts within the subnet
* Naming structure
* Domain structure
### **How can we enumerate?**
There are many tools that can be used for enumeration. Linux OS machines typically have many of these tools pre-installed.

Tools include:
* `nmap`
* `nbtscan`