## **AD Domain**

### **What is an AD Domain?**
A domain is a group of objects. In a domain, almost everything is an object. A group, a user, a hardware device such as a printer or computer is an object. By being contained within a domain, all these objects share a common admin, security, and replication settings.
### **What is the job of the AD Domain?**
The domain controls and manages authentication, authorization, name resolution and centralized management of objects​. The jobs typically handled by an IT team, is delegated to the domain. To take care of these tasks, each domain is equipped with a domain admin and an domain controller.
### **Domain Admin (DA)**
The domain admin is a user that can edit information in an AD. It is a form of administrator account. This user(s) can modify content, add and delete users, and alter user permissions. As a DA, this user holds top permissions.
### **Domain Controller (DC)**
The domain controller is the computer that contains the data that determines and validates access to an entire network. It checks usernames, passwords, and other credentials of users, allowing or denying them access.

This data that it stores is kept organized and secure. All domain admins must go through the domain controller to get information.