## **AD Structure**
### **An AD has three main components:**
**Domains**
* This is a group of objects (printers, computers, etc.) within the immediate network  

**Trees**
* A group of domains is called a tree. The domains within a tree are arranged in a hierarchy to establish secure connections between each one.  

**Forests**
* Similar to those in nature, a collection of trees is called a forest. Forests allow admin access for all trees (and thereby all domains) within it.

### Our main focus will be the smallest structure, a domain