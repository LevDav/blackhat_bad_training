## **Windows Active Directory**

### **What is a Windows Active Directory?**
A Windows Active Directory (AD) is a large database that stores how users are connected to the various network resources within an environment.
* An environment can be a single computer or a system of computers​

### **What is in an AD?**
An AD contains information such as:
* Users and computers within the environment/network​
* Permissions of users

### **Why an AD?**
An AD is mainly used to enhance security and keep an organization or business orgainzed. Through an AD, a company can set group policies, utilize single sign-on, have access to share drives, and is easily scalable.