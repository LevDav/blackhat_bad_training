# **Module 1**

## **Scenario:**
* Access is allowed on the network through a dropbox/compromised machine
* You have no user credentials

## **Objective:**
Gain domain admin access

## **Concepts**
* [Windows Active Directory (AD)](./3_Windows_AD.md)
* [Active Directory Structure](./4_AD_Structure.md)
* [Active Directory Domain](./5_AD_Domain.md)
* [Enumeration](./6_Enumeration.md)
* [Name Resolution](./7_Name_Resolution.md)
* [LLMNR](./8_LLMNR.md)
* [Windows Hashes](./9_Hashes.md)
* [LDAP](./10_LDAP.md)
* [LSASS](./11_LSASS.md)