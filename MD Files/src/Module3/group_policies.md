## **Group Policies**

### **Purpose of Group Policy**
Not everyone on the domain needs to have the same access. Group policies allows one to group computers, users, services and more and apply specific policies that affect the whole group​. Group policies also can be used to create hierarchies and only give access to the right people and machines​.

### **Group Policy Preferences**
Microsoft added the functionality of Group Policy Preferences to group policy. It allows a user to to set scheduled tasks and local admin password changing.

### **Exploitation**
All information about credentials is stored in the SYSVOL shared server, which is accessible by everyone on the AD. And even though all passwords are encrypted, the AES key that is used to encrypt them is the same and is made public information.

If a password is captured, it can be easily decrypted with the public AES key.