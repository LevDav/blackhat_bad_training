# **Module 3**

## **Scenario:**
* Log into Windows machine with AD credentials

## **Objective**
Gain domain admin access

## **Concepts**
* [SYSVOL](./sysvol_1.md)
* [Group Policies](./group_policies.md)
* [Covert LSASS Dump](./covert_lsass.md)
* [GPO Modification](./gpo_modification.md)
* [LAPS](./laps.md)
* [Shadow Copy](./shadow_copy.md)