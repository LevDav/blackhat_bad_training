## **Shadow Copy**

### **What is a Shadow Copy?**
A shadow copy is a copy of all the files on a device that are extracted and placed onto a separate machine. This can be done by a user with admin privileges on the initial device.

### **Exploitation**
Lots of useful information can be gathered from a shadow copy. And though it may be encrypted, hashes can later be cracked.

* Example:
    * `registry/SYSTEM` and `registry/SECURITY` hold hashes that can be used to log into a system.