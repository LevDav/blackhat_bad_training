## **GPO Modification**

### **What is GPO?**
A GPO (Group Policy Object) is a virtual collection of policy settings. These are where group policy settings are contained.

### **Modifying GPO**
If a user on the AD has the ability to change the GPO, that user is able to do a lot of modifications to the policies. Computers and users can be added to groups. Permissions may be increased for certain users.

*This is a good target of attack for hackers/pentesters.*