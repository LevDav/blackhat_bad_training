## **Covert LSASS**

### **LSASS vs Covert LSASS**
LSASS, as discussed in Module 1, holds the user credentials in a process. We also how it can be exploited using a procdump and `wmiexec`. It is relatively undetectable as long as Windows's pre-installed security, Windows Defender, is not turned on. 

However, once Windows Defender is activated, it easily detects when a user/attacker drops an executable or other file onto the machine it is defending.

*So how do you acquire LSASS, while Windows Defender is still active?*

### **Acquiring LSASS Covertly/Exploitation**
To acquire LSASS without setting off Windows Defender, a user must use a shared folder.

A share folder with the `procdump` and `wmiexec` executables is created on a machine within the AD with the LSASS desired. Using credentials, (in this scenario acquired from SYSVOL) log into the target computer, and mount the shared folder just created onto the target machine. The executable can now be run to gather the LSASS process onto the shared folder. Once data is gathered, the shared drive may be deleted from the target computer. 

Thus, you have covertly captured data without alerting Windows Defender.