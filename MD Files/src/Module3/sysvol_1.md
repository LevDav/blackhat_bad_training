## **SYSVOL**

### **What is SYSVOL?**
SYSVOL is a shared file server that can be used to share information across computers. Any computer has the ability to start a file server.

All AD users have read access to this share located in the domain controllers. SYSVOL also contains logon scripts, group policy data and other information that pertains to a Domain Controller​.