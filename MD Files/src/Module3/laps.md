## **LAPS**

### **Purpose of LAPS**
LAPS (Local Administrator Password Solution) is used to manage passwords for local administrator accounts. This management of passwords is done through an AD. 

### **Exploitation**
If the system is misconfigured, a user can use LAPS to gain credentials to the local admin on the domain controller, gaining ultimate privileges.